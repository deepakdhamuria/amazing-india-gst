<?php

/**
 * Plugin Name:       Amazing India GST
 * Plugin URI:        https://amazingworkz.com/amazing-india-gst
 * Description:       Manage GST settings of your shop and products
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * WC requires at least: 4.0
 * WC tested up to:   5.7.1
 * Author:            Amazing Workz Studios
 * Author URI:        https://amazingworkz.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 */

//Die if someone accessed directly except Page Admin
defined('ABSPATH') || die("You are not allowed");

add_action('plugins_loaded', 'amzgst_check_for_woocommerce');

function amzgst_no_wc_admin_notice() {
    echo '<div class="notice notice-error is-dismissible">
          <p>Woocommerce is not active, Amazing India GST needs Wocommerce to work properly</p>
         </div>';
}

function amzgst_check_for_woocommerce() {
    if (!defined('WC_VERSION')) {
        add_action( 'admin_notices', 'amz_no_wc_admin_notice' );
    } else {
        $gst_settings = new amazing_WC_GST_Settings();
        $GstSettingsMenu = new amazing_GstSettingsMenu();
        $gst_settings->init();
    }
}

class amazing_WC_GST_Settings
{
    public function init()
    {
        add_action('woocommerce_product_options_general_product_data', array( $this , 'amz_fn_add_product_custom_meta_box') );
        add_action('woocommerce_process_product_meta', array( $this , 'amz_fn_save_hsn_code_field') );
        add_action('woocommerce_email_after_order_table', array( $this, 'amz_fn_show_admin_gstin_number_field_order' ));
        add_filter('woocommerce_order_formatted_line_subtotal', array($this,'amz_fn_show_itemized_tax_email'), 99, 3 );
        add_action('woocommerce_order_item_meta_start', array($this, 'amz_fn_show_product_hsn_code_email'), 10, 3);
    }
    function amz_fn_add_product_custom_meta_box()
    {
        woocommerce_wp_text_input( 
            array( 
                'id'            => 'product_hsn_code', 
                'label'         => __('HSN Code', 'woocommerce' ), 
                'description'   => __( 'HSN Code is mandatory for GST', 'woocommerce' ),
                'custom_attributes' => array( 'required' => 'required' ),
                'value'         => get_post_meta( get_the_ID(), 'product_hsn_code', true )
            )
        );
    }

    function amz_fn_save_hsn_code_field( $post_id )
    {
        $value = ( $_POST['product_hsn_code'] )? sanitize_text_field( $_POST['product_hsn_code'] ) : '' ;
        update_post_meta( $post_id, 'product_hsn_code', $value );
    }
    
    // GST Emails Settings

    function amz_fn_show_admin_gstin_number_field_order()
    {
        global $wpdb;
        $results_of_gstin_num = $wpdb->get_results( "SELECT option_value FROM $wpdb->options WHERE option_name LIKE 'amazing_gstin';" );
        foreach($results_of_gstin_num as $results_gstin_num)
        {
            $results_gstin_number = $results_gstin_num->option_value;
        }
        if(isset($results_gstin_number))
        {
            echo '<p><strong>GSTIN: '. $results_gstin_number .'</strong></p>' ;
        }
    }

    function amz_fn_show_itemized_tax_email($subtotal, $item, $order) {
        $show_itemised = get_option( 'show_itemised_tax_email', true );
        if( $show_itemised != "yes" ) return $subtotal;

        $taxes = $order->get_taxes();
        $arr_tax = array();
        $symbol = get_woocommerce_currency_symbol();
        
        foreach($taxes as $tax ){    
            $tax_data = $tax->get_data();
            $price = $tax_data['tax_total'];
            $taxes_rate_id = $tax_data['rate_id'];
            $taxes_label = $tax_data['label'];
            $arr_tax[$taxes_rate_id] = array(
                    'label' => $taxes_label,
                    'cost' => $price
                );
        }

        $txn = $item->get_data()['taxes']['total'];
        $price = "";
        foreach ($txn as $tax_key => $value) {
            if(!empty($value)){
                if (array_key_exists($tax_key,$arr_tax)){
                    $value = number_format((float)$value, 2, '.', '');
                    $price .= $symbol.$value . "(".$arr_tax[$tax_key]['label'].")" . "<br>";
                }
            }
        }
        return $subtotal . "<br>" . $price;
    }
    
    function amz_fn_show_product_hsn_code_email( $item_id, $item, $order ) {
        if ($item->is_type('line_item') ) {
            $hsn_code = get_post_meta( $item->get_product_id(), 'product_hsn_code', true );
    
            if ( ! empty($hsn_code) ) {
                printf( '<div><small>' . __("HSN: %s", "woocommerce") . '</small></div>', $hsn_code );
            }
        }
    }
}

//Add a new GST settings tab.
class amazing_GstSettingsMenu
{
    function __construct()
    {
        // Add an admin menu named as GST Settings
        add_action('admin_menu',array($this,'gst_tax_class_menu'));
    }
    function gst_tax_class_menu()
    {
        add_menu_page('GST Settings','GST Settings','manage_options',__FILE__,array($this,'gst_tags_settings'),"dashicons-tag",30);   
    }
    //Perform action
    function gst_tags_settings()
    {
        include_once(__DIR__. '/gst-data.php');
    }
}
