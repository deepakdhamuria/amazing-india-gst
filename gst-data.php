<?php session_start(); 
  defined('ABSPATH') || exit;
  define('AMZ_INDIA_GST_PLUGIN_URL', plugin_dir_url(__FILE__));
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>GST Settings</title>
  <link href="<?php echo AMZ_INDIA_GST_PLUGIN_URL; ?>vendor/css/select2.min.css" rel="stylesheet" />
  <script src="<?php echo AMZ_INDIA_GST_PLUGIN_URL; ?>vendor/js/select2.min.js"></script>
  <script type="text/javascript">
      function selectTaxSlab(taxName) {
          var taxSlabs = document.getElementById( 'gst_class_select_slab' );

          for ( var i = 0; i < taxSlabs.options.length; i++ ) {
              var option = taxSlabs.options[i];
              if ( taxName == o.text ) {
                option.selected = true;
              }
          }
      }
  </script>
  <style>

    div.updated p
    {
      margin: .5em 0;
      margin-top: 0.5em;
      margin-right: 0px;
      margin-bottom: 0.5em;
      margin-left: 0px;
      padding: 2px;
      padding-top: 2px;
      padding-right: 2px;
      padding-bottom: 2px;
      padding-left: 2px;
    }
  </style>
</head>
<body>
  <div class="wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1><hr/>
    <?php
    if(isset($_SESSION['status']))
    { 
    ?>
      <div id="message" class="updated inline"><p><strong>
      <?php
      echo $_SESSION['status'];
      ?>
      </strong></p></div>
      <?php
      unset($_SESSION['status']);
    }
    ?>
    <form action="" method="post" name="gst_tax_form" id="gst_tax_form" enctype="multipart/form-data">
      <table class="form-table">
        <!-- GST Number -->
        <tr valign="top-1">
          <th scope="row" class="label-1">
            <label for="gst_class_gstin_number">GSTIN</label>
          </th>
          <td class="forminp text_field-1">
            <input
            name="gst_class_gstin_number"
            id="gst_class_gstin_number"
            class=""
            type="text"
            style="min-width:400px;"
            placeholder=""
            required="required" 
            <?php
            global $wpdb;
            $results_of_option_name = $wpdb->get_results( "SELECT option_name FROM $wpdb->options WHERE option_name LIKE 'amazing_gstin';" );
            foreach($results_of_option_name as $results_option_name)
                {
                  $is_exist_option_name = $results_option_name->option_name;
                }
            /* Get recently GST Number to the GST text box if amazing_gstin option name is not exists in db */
            if(!isset($is_exist_option_name))
            {
              ?>
              value="<?php
              /* Get recently inserted GST Number to the GST text box when onclick the Save button only*/
              if(isset($_POST['gst_class_gstin_number']))
              {
                echo $_POST['gst_class_gstin_number'];
              }
              ?>"
            <?php
            }
            /* Get recently GST Number to the GST text box if amazing_gstin option name is exists in db */
            elseif(isset($is_exist_option_name))
            {
              $results_of_gstin_num = $wpdb->get_results( "SELECT option_value FROM $wpdb->options WHERE option_name LIKE 'amazing_gstin';" );
              foreach($results_of_gstin_num as $result_gstin_num)
              {
                if(!empty($result_gstin_num))
                {
                  ?>
                  value="<?php 
                  /* Get recently inserted GST Number to the GST text box when onclick the Save button only*/
                  if(isset($_POST['gst_class_gstin_number']))
                  {
                    echo esc_attr($_POST['gst_class_gstin_number']);
                  }
                  /* Get recently inserted GST Number to the GST text box when refresh the page only */
                  else
                  {
                    $last_inserted_gstin_num = $result_gstin_num->option_value;
                    echo $last_inserted_gstin_num;
                  }
                  ?>"
            <?php
                }
              }
            }  
            ?>
            /> <p class="description">This GSTIN number can be displayed in your email and on your invoice.</p>
          </td>
        </tr>

        <!-- Store location state -->
        <tr valign="top-2">
          <th scope="row" class="label-2">
            <label for="gst_store_country_state">Store location state </label>
          </th>
          <td class="forminp text_field-2">
            <select
            name="gst_store_country_state"
            id="gst_store_country_state"
            class=""
            style="min-width:400px;height:auto;"
            disabled="disabled"   
            required="required"    /> 
          
            <option value="AP">India -- Andhra Pradesh</option>
            <option value="AR">India -- Arunachal Pradesh</option>
            <option value="AS">India -- Assam</option>
            <option value="BR">India -- Bihar</option>
            <option value="CT">India -- Chhattisgarh</option>
            <option value="GA">India -- Goa</option>
            <option value="GJ">India -- Gujarat</option>
            <option value="HR">India -- Haryana</option>
            <option value="HP">India -- Himachal Pradesh</option>
            <option value="JK">India -- Jammu and Kashmir</option>
            <option value="JH">India -- Jharkhand</option>
            <option value="KA">India -- Karnataka</option>
            <option value="KL">India -- Kerala</option>
            <option value="LA">India -- Ladakh</option>
            <option value="MP">India -- Madhya Pradesh</option>
            <option value="MH">India -- Maharashtra</option>
            <option value="MN">India -- Manipur</option>
            <option value="ML">India -- Meghalaya</option>
            <option value="MZ">India -- Mizoram</option>
            <option value="NL">India -- Nagaland</option>
            <option value="OR">India -- Odisha</option>
            <option value="PB">India -- Punjab</option>
            <option value="RJ">India -- Rajasthan</option>
            <option value="SK">India -- Sikkim</option>
            <option value="TN">India -- Tamil Nadu</option>
            <option value="TS">India -- Telangana</option>
            <option value="TR">India -- Tripura</option>
            <option value="UK">India -- Uttarakhand</option>
            <option value="UP">India -- Uttar Pradesh</option>
            <option value="WB">India -- West Bengal</option>
            <option value="AN">India -- Andaman and Nicobar Islands</option>
            <option value="CH">India -- Chandigarh</option>
            <option value="DN">India -- Dadra and Nagar Haveli</option>
            <option value="DD">India -- Daman and Diu</option>
            <option value="DL">India -- Delhi</option>
            <option value="LD">India -- Lakshadeep</option>
            <option value="PY">India -- Pondicherry (Puducherry)</option>
            </select>
            <?php
            global $wpdb;
            $results_of_option_name = $wpdb->get_results( "SELECT option_name FROM $wpdb->options WHERE option_name LIKE 'amazing_gst_state';" );
            foreach($results_of_option_name as $results_option_name)
                {
                  $is_exist_option_name = $results_option_name->option_name;
                }
            /* Get recently selected country or state name to the country or state dropdown box if amazing_gst_state option name is not exists in db */
            if(!isset($is_exist_option_name))
            {
              /* Execute when onclick the Save button */
              if(isset($_POST['gst_store_country_state']))
              {
                $last_inserted_country_state = $_POST['gst_store_country_state'];
                ?>
                <script type="text/javascript">
                  var last_inserted_country_state_code = "<?php echo esc_attr($last_inserted_country_state); ?>";
                  document.getElementById("gst_store_country_state").value=last_inserted_country_state_code;
                </script>
                <?php
              }
            }
            /* Get recently selected country or state name to the country or state dropdown box if amazing_gst_state option name is exists in db */
            elseif(isset($is_exist_option_name))
            {
              /* Execute when refresh the page */
              if(!isset($_POST['gst_store_country_state']))
              {
                $results_of_country_state = $wpdb->get_results( "SELECT option_value FROM $wpdb->options WHERE option_name LIKE 'amazing_gst_state';" );
                foreach($results_of_country_state as $result_country_state)
                {
                  if(!empty($result_country_state))
                  {
                    $last_inserted_country_state = $result_country_state->option_value;
                  }
                }
                ?>
              <script type="text/javascript">
                var last_inserted_country_state_code = "<?php echo esc_attr($last_inserted_country_state)?>";
                document.getElementById("gst_store_country_state").value=last_inserted_country_state_code;
              </script>
              <?php
              }
              /* Execute when onclick the Save button */
              elseif(isset($_POST['gst_store_country_state']))
              {
                $last_inserted_country_state = esc_attr($_POST['gst_store_country_state']);
                ?>
                <script type="text/javascript">
                  var last_inserted_country_state_code = "<?php echo esc_attr($last_inserted_country_state); ?>";
                  document.getElementById("gst_store_country_state").value=last_inserted_country_state_code;
                </script>
                <?php
              }
            } ?>
      
            <input type="checkbox" id="ckbx" onclick="Enabling(this)"/>Edit
          </td>
        </tr>

        <!-- GST Tax Slabs -->
        <tr valign="top-3">
          <th scope="row" class="label-3">
            <label for="gst_class_select_slab">Select Tax Slab(s)</label>
          </th>
          <td class="forminp text_field-3"> 
            <select 
            data-placeholder="Select Tax Slabs..."
            multiple
            name="gst_class_select_slab[]"
            id="gst_class_select_slab"
            class=""
            style="min-width:400px;height:auto;"
            required="required"    />

            <option value="1%">1%</option>
            <option value="2%">2%</option>
            <option value="3%">3%</option>
            <option value="4%">4%</option>
            <option value="5%">5%</option>
            <option value="6%">6%</option>
            <option value="7%">7%</option>
            <option value="8%">8%</option>
            <option value="9%">9%</option>
            <option value="10%">10%</option>
            <option value="11%">11%</option>
            <option value="12%">12%</option>
            <option value="13%">13%</option>
            <option value="14%">14%</option>
            <option value="15%">15%</option>
            <option value="16%">16%</option>
            <option value="17%">17%</option>
            <option value="18%">18%</option>
            <option value="19%">19%</option>
            <option value="20%">20%</option>
            <option value="21%">21%</option>
            <option value="22%">22%</option>
            <option value="23%">23%</option>
            <option value="24%">24%</option>
            <option value="25%">25%</option>
            <option value="26%">26%</option>
            <option value="27%">27%</option>
            <option value="28%">28%</option>
            <option value="29%">29%</option>
            <option value="30%">30%</option>
            
            
            <?php
            global $wpdb;
            $results_of_option_name = $wpdb->get_results( "SELECT option_name FROM $wpdb->options WHERE option_name LIKE 'amazing_gst_tax';" );
            foreach($results_of_option_name as $results_option_name)
                {
                  $is_exist_option_name = $results_option_name->option_name;
                }
            /* Get recently selected tax slabs if amazing_gst_tax option name is not exists in db */
            if(!isset($is_exist_option_name))
            {
              /* Execute when onclick the Save button */
              if(isset($_POST['gst_class_select_slab']))
              {
                /* Get recently selected tax slabs to the multile tax slabs box */
                $recently_inserted_tax_slab_s = esc_attr($_POST['gst_class_select_slab']);
                foreach($recently_inserted_tax_slab_s as $result_tax_name)
                {
                  ?>
                  <script type="text/javascript">
                    var result_tax_name = "<?php echo $result_tax_name;?>";
                    jQuery.each(result_tax_name.split(","), function(i,e){
                      jQuery("#gst_class_select_slab option[value='" + e + "']").prop("selected", true);
                    });
                  </script>
                  <?php
                }
              }
            }
            /* Get recently selected tax slabs if amazing_gst_tax option name is exists in db */
            elseif(isset($is_exist_option_name))
            {
              /* Execute when refresh the page */
              if(!isset($_POST['gst_class_select_slab']))
              {
                $results_of_tax_name = $wpdb->get_results( "SELECT option_value FROM $wpdb->options WHERE option_name LIKE 'amazing_gst_tax';" );
                foreach($results_of_tax_name as $result_tax_name)
                {
                  if(!empty($result_tax_name))
                  {
                    $last_inserted_tax_slab_s = $result_tax_name->option_value;
                  }
                }
                ?>
              <script type="text/javascript">
                var result_tax_name = "<?php echo $last_inserted_tax_slab_s;?>";
                jQuery.each(result_tax_name.split(","), function(i,e){
                  jQuery("#gst_class_select_slab option[value='" + e + "']").prop("selected", true);
                });
              </script>
              <?php
              }
              /* Execute when onclick the Save button */
              elseif(isset($_POST['gst_class_select_slab']))
              {
                /* Get recently selected tax slabs to the multile tax slabs box */
                $recently_inserted_tax_slab_s = $_POST['gst_class_select_slab'];
                foreach($recently_inserted_tax_slab_s as $result_tax_slabs)
                {
                  ?>
                  <script type="text/javascript">
                    var result_tax_name = "<?php echo $result_tax_slabs?>";
                    jQuery.each(result_tax_name.split(","), function(i,e){
                      jQuery("#gst_class_select_slab option[value='" + e + "']").prop("selected", true);
                    });
                  </script>
                  <?php
                }
              }
            } ?>
            </select>
          </td>
        </tr>
      </table>
      <p class="submit">
        <button name="save" id="save" class="button-primary GST-class-save-button" type="submit" value="Save changes")>Save changes</button>
      </p>
    </form>
  </div>
</body>
</html>
<script type="text/javascript">
      function Enabling(ckbx) {
          var selected_value = document.getElementById("gst_store_country_state");
          selected_value.disabled = ckbx.checked ? false : true;
          if (!selected_value.disabled) {
              selected_value.focus();
          }
      }

      jQuery(document).ready(function ($) {
          // Initialize select2
          $("#gst_store_country_state").select2();
          $("#gst_class_select_slab").select2();

          $("#save").click(function () {

              var tax_values = document.gst_tax_form.gst_class_select_slab.value;
              if (tax_values == null || tax_values == "") {
                  alert("Tax Slabs can't be blank");
                  return false;
              }
              var GST_gstin_number = document.getElementById("gst_class_gstin_number").value
              var selected_slab_or_s = [];
              for (var option of document.getElementById('gst_class_select_slab').options) {
                  if (option.selected) {
                      selected_slab_or_s.push(option.value);
                  }
              }

              $('#gst_store_country_state').find("option:selected").each(function () {
                  var selected_state_code = $(this).val();

                  $.ajax({
                      method: "POST",
                      data:
                      {
                          GST_gstin_number: GST_gstin_number,
                          selected_slab_or_s: selected_slab_or_s,
                          country_or_state_code: selected_state_code
                      },
                      success: function (data) {
                          console.log("happy")
                      }
                  });
              });
          });
      });
</script>
<?php
if(isset($_POST['GST_gstin_number']) && isset($_POST['selected_slab_or_s']) && isset($_POST['country_or_state_code']))
{
  global $wpdb;
  $gstin_number=$_POST['GST_gstin_number'];
  $selected_slab_s=$_POST['selected_slab_or_s'];
  $country_or_state=$_POST['country_or_state_code'];
  $selected_slab_s_to_save = implode(',', $_POST['selected_slab_or_s']);

  if($country_or_state!=NULL && $gstin_number!=NULL){   
    //db tables prifix
    $table_gst_tags_tax_rate_classes_prefix = $wpdb->prefix . "options";
    $table_woo_commerce_tax_rate_prefix = $wpdb->prefix . "woocommerce_tax_rates";
    $table_woo_commerce_tax_rate_classes_prefix = $wpdb->prefix . "wc_tax_rate_classes";

    $wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE 'amazing_gstin';" );
    $wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE 'amazing_gst_state';" );
    $wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE 'amazing_gst_tax';" );

    // Store data in options Table
    $success=$wpdb->insert($table_gst_tags_tax_rate_classes_prefix,array('option_name' => "amazing_gstin", 'option_value' => $gstin_number, 'autoload' => "yes"),array('%s','%s','%s'));
    $success=$wpdb->insert($table_gst_tags_tax_rate_classes_prefix,array('option_name' => "amazing_gst_state", 'option_value' => $country_or_state, 'autoload' => "yes"),array('%s','%s','%s'));
    $success=$wpdb->insert($table_gst_tags_tax_rate_classes_prefix,array('option_name' => "amazing_gst_tax", 'option_value' => $selected_slab_s_to_save, 'autoload' => "yes"),array('%s','%s','%s'));

    foreach($selected_slab_s as $selected_tax_slab)
    {
      $slab_name = preg_replace('/%/', '', $selected_tax_slab);
      $state_tax ='';
      $slab_name = (int)$slab_name;
      $state_tax = $slab_name / 2;

      $state = get_option("gst_store_country_state");
      $ut_state = array('CH','AN','DN','DD', 'LD');

      if( isset( $state ) ) :
        $tax_slab_row_cgst = $state_tax."% CGST";
        $tax_slab_row_utgst = $state_tax."% UTGST";
        $tax_slab_row_sgst = $state_tax."% SGST";
        $tax_slab_row_igst = $slab_name."% IGST";

        $select_table_tax_cgst = $wpdb->get_var("SELECT tax_rate_id FROM $table_woo_commerce_tax_rate_prefix WHERE tax_rate_name='$tax_slab_row_cgst'");

        $select_table_tax_utgst = $wpdb->get_var("SELECT tax_rate_id FROM $table_woo_commerce_tax_rate_prefix WHERE tax_rate_name='$tax_slab_row_utgst'");

        $select_table_tax_sgst = $wpdb->get_var("SELECT tax_rate_id FROM $table_woo_commerce_tax_rate_prefix WHERE tax_rate_name='$tax_slab_row_sgst'");

        $select_table_tax_igst = $wpdb->get_var("SELECT tax_rate_id FROM $table_woo_commerce_tax_rate_prefix WHERE tax_rate_name='$tax_slab_row_igst'");

        //Store data in WooCommerce Table
        $wpdb->insert($table_woo_commerce_tax_rate_classes_prefix,array( 'name' => $selected_tax_slab, 'slug' => $slab_name),array( '%s'));

        if( ($select_table_tax_cgst == NULL || empty($select_table_tax_cgst)) )
        {
          // Store data in WooCommerce Table
          $wpdb->insert($table_woo_commerce_tax_rate_prefix,array( 'tax_rate_country' => "IN", 'tax_rate_state' => $country_or_state,'tax_rate' => $state_tax,'tax_rate_name' => $state_tax."% CGST",'tax_rate_priority' => 1,'tax_rate_compound' => 0,'tax_rate_shipping' => 0,'tax_rate_order' => 0,'tax_rate_class' =>$slab_name),array( '%s','%s','%s','%s','%d','%d','%d','%d','%s'));
        }

        if(in_array($state, $ut_state))
        {
          if( ($select_table_tax_utgst == NULL || empty($select_table_tax_utgst)) )
          {
            // Store data in WooCommerce Table
            $wpdb->insert($table_woo_commerce_tax_rate_prefix,array( 'tax_rate_country' => "IN", 'tax_rate_state' => $country_or_state,'tax_rate' => $state_tax,'tax_rate_name' => $state_tax."% UTGST",'tax_rate_priority' => 2,'tax_rate_compound' => 0,'tax_rate_shipping' => 0,'tax_rate_order' => 0,'tax_rate_class' =>$slab_name),array( '%s','%s','%s','%s','%d','%d','%d','%d','%s'));
          }    
        } else 
        {
          if( ($select_table_tax_sgst == NULL || empty($select_table_tax_sgst)) )
          {
            // Store data in WooCommerce Table
            $wpdb->insert($table_woo_commerce_tax_rate_prefix,array( 'tax_rate_country' => "IN", 'tax_rate_state' => $country_or_state,'tax_rate' => $state_tax,'tax_rate_name' => $state_tax."% SGST",'tax_rate_priority' => 2,'tax_rate_compound' => 0,'tax_rate_shipping' => 0,'tax_rate_order' => 0,'tax_rate_class' =>$slab_name),array( '%s','%s','%s','%s','%d','%d','%d','%d','%s'));
          }    
        }
        if( ($select_table_tax_igst == NULL || empty($select_table_tax_igst)) )
        {
          // Store data in WooCommerce Table
          $wpdb->insert($table_woo_commerce_tax_rate_prefix,array( 'tax_rate_country' => "IN", 'tax_rate_state' => $country_or_state,'tax_rate' => $slab_name,'tax_rate_name' => $slab_name."% IGST",'tax_rate_priority' => 1,'tax_rate_compound' => 0,'tax_rate_shipping' => 0,'tax_rate_order' => 0,'tax_rate_class' =>$slab_name),array( '%s','%s','%s','%s','%d','%d','%d','%d','%s'));
        }
      endif;
      if($success)
      {
        $_SESSION['status'] = "Your settings have been saved.";
        header("Location: admin.php?page=GST-settings%2FGST-settings.php");
      }
      else
      {
        $_SESSION['status'] = "Your settings hasn't saved.";
        header("Location: admin.php?page=GST-settings%2FGST-settings.php");
      }
    }
  }
}
?>
