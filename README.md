# Amazing India GST
 - Contributors: Amazing Workz Technology
 - Tags: gst, woocommerce, addon, woocommerce addon, gst tax, woocommerce tax, indian gst tax, HSN, HSN code, HSN code woocommerce  
 - Requires at least: 5.2
 - Requires PHP : 7.2
 - Tested up to: 5.8.1
 - Stable tag: 1.0.0
 - License: GPLv2 or later
 - License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin is for managing India GST taxes. This allows you to set GSTIN, add taxes automatically and add HSN code to products.

### Description

Using Amazing India GST plugin, you can add a GSTIN for your shop and HSN code for products which will be shown on order emails sent to customers. You can also specify tax slabs and it will auto generate IGST, CGST and SGST taxes for you. 

### Features

 - Admin can add GSTIN, which will be shown in order email.
 - Admin can specify the state code where store is located.
 - Admin can enter HSN code for products which is shown in email.
 - Meta field (product_hsn_code) for product can be used to show on reciepts genearted using other plugins.
 - Admin can choose required tax slabs, which will generate tax slabs automatically with IGST, CGST and SGST.

[Visit plugin site](https://amazingworkz.com/amazing-india-gs)

### Installation

```sh
 1. Upload `amazing-india-gst` zip to the `/wp-content/plugins/` directory
 2. Activate the plugin through the 'Plugins' menu in WordPress
 3. Configure GST Settings
```

### Changelog
```sh
1.0.0
 - Initial release of plugin.
```
